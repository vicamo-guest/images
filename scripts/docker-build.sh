#!/bin/bash

set -eu

DOCKER_IMAGE_NAME=$1

VOLUMES="-v $PWD/..:/tmp/building -v $PWD:/tmp/building/package"

CONTAINER_ID=$(docker run -d --rm -w /tmp/building/package ${VOLUMES} ${DOCKER_IMAGE_NAME} sleep infinity)

cleanup() {
    docker rm -f ${CONTAINER_ID}
}
trap cleanup EXIT

DEBIAN_VARENVS=""

IFS=$'\n' && for varenv in $(env); do
    if [[ $varenv == DEB* ]]; then
        DEBIAN_VARENVS+=" -e \"${varenv}\""
    fi
done

set -x

eval docker cp /etc/apt/sources.list.d/./ ${CONTAINER_ID}:/etc/apt/sources.list.d/

eval docker cp /etc/apt/preferences.d/./ ${CONTAINER_ID}:/etc/apt/preferences.d/

eval docker exec ${DEBIAN_VARENVS} ${CONTAINER_ID} apt-get update

eval docker exec ${DEBIAN_VARENVS} ${CONTAINER_ID} apt-get install eatmydata -y

eval docker exec ${DEBIAN_VARENVS} ${CONTAINER_ID} eatmydata apt-get dist-upgrade -y

eval docker exec ${DEBIAN_VARENVS} ${CONTAINER_ID} eatmydata apt-get install dpkg-dev build-essential -y

eval docker exec ${DEBIAN_VARENVS} ${CONTAINER_ID} eatmydata apt-get build-dep -y .

eval docker exec ${DEBIAN_VARENVS} ${CONTAINER_ID} eatmydata chown -R salsa-ci:0 /tmp/building

eval docker exec --user salsa-ci ${DEBIAN_VARENVS} ${CONTAINER_ID} eatmydata dpkg-buildpackage "${@:2}"
